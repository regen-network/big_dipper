#!/bin/bash

OUTPUT=../big_dipper-deploy/deploy-buildpack
DOCKER=../big_dipper-deploy/deploy-docker

echo "Building for production..."
# meteor build ../output/ --architecture os.linux.x86_64 --server-only

# Assume we are in bigdipper directory
mkdir $OUTPUT
mkdir $DOCKER

# Clean old build
rm $OUTPUT/big_dipper.tar.gz
rm $DOCKER/big_dipper.tar.gz
rm -rf $DOCKER/deploy-docker/bundle

# Build
meteor build $OUTPUT/. --server-only --architecture os.linux.x86_64
cp $OUTPUT/big_dipper.tar.gz $DOCKER/.
cd $DOCKER && tar -xf big_dipper.tar.gz  && rm big_dipper.tar.gz

# Build Docker Image
#cd $DOCKER
#docker login registry.gitlab.com
#docker build -t registry.gitlab.com/regen-network/big_dipper .
#docker push registry.gitlab.com/regen-network/big_dipper:latest

# Note: the following commands can be used to deploy to cf

# Push to cf
#cd $OUTPUT
#cf push 

# Push to cf with docker
#cd $DOCKER
#cf push


